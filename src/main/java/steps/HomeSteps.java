package steps;

import cucumber.api.java.pt.Dado;
import org.openqa.selenium.WebDriver;
import page.HomePageActions;
import utils.DriverSetUp;

public class HomeSteps {

    private WebDriver driver;

    @Dado("^Acessei a pagina$")
    public void acessei_a_pagina() throws Throwable {
        new DriverSetUp().setUP();
        driver = DriverSetUp.getDriver();
        driver.navigate().to("https://www.sicredi.com.br/html/ferramenta/simulador-investimento-poupanca/");

        HomePageActions setValues = new HomePageActions();
        setValues.setRadioPerfil("paraVoce");
        setValues.setarValorAplicar("8000");
        setValues.setarValorInvestir("15000");
        setValues.setarTempo("12");
    }
}
